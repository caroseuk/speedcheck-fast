
## Install

Ensure you have [Node.js](https://nodejs.org) version 8+ installed. Then run the following:

```
$ npm install --global speedcheck-fast
```


## Usage

```
$ speedcheck-fast --help

  Usage
    $ speedcheck-fast

  Options
    --json -j     Output the result as JSON
    --bytes -b    Output the result in megabytes per second (MBps)
    --verbose -v  Output more detailed information
```

